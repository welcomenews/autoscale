# Provider
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      #version = ">= 0.13"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_region
}

data "yandex_iam_service_account" "for-autoscale" {
  name = "for-autoscale"
}

## network
resource "yandex_vpc_network" "vm-autoscale-network" {
  name = "internal-2"
}

resource "yandex_vpc_subnet" "vm-autoscale-subnet-b" {
  name           = "internal-b"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-autoscale-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}


resource "yandex_vpc_security_group" "sg-1" {
  name                = "sg-autoscale"
  network_id          = yandex_vpc_network.vm-autoscale-network.id
  egress {
    protocol          = "ANY"
    description       = "any"
    v4_cidr_blocks    = ["0.0.0.0/0"]
  }
  ingress {
    protocol          = "TCP"
    description       = "ext-http"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 80
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
}


## instance 1
resource "yandex_compute_instance" "vm-1" {
  name        = "skillbox-vm1"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :>consul_instances;
        echo [consul_instances] >> consul_instances;
        echo ${yandex_compute_instance.vm-1.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-1.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-1.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=server consul_bootstrap_expect=true >> consul_instances;
        :>ip;
        echo ${yandex_compute_instance.vm-1.network_interface.0.ip_address} >> ip
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
}

## instance 2
resource "yandex_compute_instance" "vm-2" {
  name        = "skillbox-vm2"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.vm-2.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-2.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-2.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=server consul_bootstrap_expect=true >> consul_instances;
        echo ${yandex_compute_instance.vm-2.network_interface.0.ip_address} >> ip
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.vm-1]
}

## instance 3
resource "yandex_compute_instance" "vm-3" {
  name        = "skillbox-vm3"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.vm-3.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-3.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-3.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=server consul_bootstrap_expect=true >> consul_instances;
        echo ${yandex_compute_instance.vm-3.network_interface.0.ip_address} >> ip
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.vm-2]
}

## instance 4
resource "yandex_compute_instance" "vm-4" {
  name        = "skillbox-template"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-4.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-4.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=client consul_enable_local_script_checks=false >> consul_instances;
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} >> ip;
        :>consul_template;
        echo [consul_template] >> consul_template;
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} >> consul_template;
        :>nginx_instances;
        echo [nginx_instances] >> nginx_instances;
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} >> nginx_instances;
        :>consul_services;
        echo [consul_services] >> consul_services;
        :>tmhosts.txt;
        cat consul_instances >> tmhosts.txt;
        cat nginx_instances >> tmhosts.txt;
        cat consul_template >> tmhosts.txt
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.vm-3]
}

resource "null_resource" "install-consul" {


  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("~/.ssh/id_ed25519")
    host = "${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address}"
  }

  provisioner "local-exec" {
    command = <<EOT
      scp ./tmhosts.txt ubuntu@${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address}:/tmp;
      scp ./ssh_config ubuntu@${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address}:/tmp
    EOT
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/ssh_config /etc/ssh/",
      "sudo apt update",
      "sudo apt install python3-pip -y",
      "sudo apt install software-properties-common -y",
      "sudo add-apt-repository --yes --update ppa:ansible/ansible",
      "sudo apt install ansible -y",
      "sudo apt install git -y",
      "sudo apt install net-tools -y",
      "sudo apt install --no-install-recommends python-netaddr -y",
      "sudo apt install python-pip -y",
      "pip install python-consul",
      "git clone https://gitlab.com/welcomenews/skillbox-diploma-advanced.git",
      "cd skillbox-diploma-advanced/infra/terraform && mv /tmp/tmhosts.txt .",
      "ssh-keygen -t ed25519 -q -N '' -f ~/.ssh/id_ed25519",
      "ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address} > ipd",
      "ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.vm-2.network_interface.0.nat_ip_address}",
      "ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.vm-3.network_interface.0.nat_ip_address}",
      "ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.vm-4.network_interface.0.nat_ip_address} >> ipd",
      "ansible-playbook -e 'ansible_python_interpreter=/usr/bin/python3' -u ubuntu -i tmhosts.txt ansible/site.yml |tail -20  > outdeploy"
    ]
  }
  depends_on = [yandex_compute_instance.vm-4]
}

