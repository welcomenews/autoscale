# Provider
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      #version = ">= 0.13"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_region
}

data "yandex_iam_service_account" "for-autoscale" {
  name = "for-autoscale"
}

## network
data "yandex_vpc_network" "vm-autoscale-network" {
  name = "internal-2"
}

data "yandex_vpc_subnet" "vm-autoscale-subnet-b" {
  subnet_id     = data.yandex_vpc_network.vm-autoscale-network.id
}

data "yandex_vpc_security_group" "sg-1" {
  name                = "sg-autoscale"
##  security_group_id   = data.yandex_vpc_network.vm-autoscale-network.id
}


resource "yandex_compute_instance_group" "autoscale-group" {
    name = "autoscale-vm-ig"
    service_account_id  = data.yandex_iam_service_account.for-autoscale.id
    instance_template {

    platform_id = "standard-v3"
    resources {
#      core_fraction = 20
      memory = 2
      cores  = 2
    }
  
    boot_disk {
      initialize_params {
        image_id = "fd8ano8tvtkcdkeimscq"
        size     = 20
      }
    }

## Делает машину не прирываемой.
    scheduling_policy {
      preemptible = false 
    }

    network_interface {
      network_id = data.yandex_vpc_network.vm-autoscale-network.id
      nat = true
    }

    metadata = {
      ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
      user-data = "${file("test.yaml")}"
    }
  }

  scale_policy {
    auto_scale {
      initial_size           = 1
      measurement_duration   = 60
      cpu_utilization_target = 60
      min_zone_size          = 1
      max_size               = 3
      warmup_duration        = 60
    }
  }

  allocation_policy {
    zones = [
##      "ru-central1-a",
      "ru-central1-b"
    ]
  }

  deploy_policy {
    max_creating    = 1
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "auto-group-tg"
    target_group_description = "load balancer target group"
  }
}

# Создание сетевого балансировщика

resource "yandex_lb_network_load_balancer" "balancer" {
  name = "group-balancer"

  listener {
    name        = "http"
    port        = 80
    target_port = 80
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.autoscale-group.load_balancer.0.target_group_id
    healthcheck {
      name = "tcp"
      tcp_options {
        port = 80
      }
    }
  }
  depends_on = [yandex_compute_instance_group.autoscale-group]
}

